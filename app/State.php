<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class State extends Model
{
    protected $guarded = [];

    public function traders() {
        return $this->belongsToMany('App\Trader');
    }

    public function products() {
        return $this->belongsToMany('App\Product');
    }
}
