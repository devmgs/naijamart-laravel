<?php

namespace App\Http\Controllers\API\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use GuzzleHttp\Client;
use Exception;

class AuthController extends Controller
{
    public function login(Request $request)
    {
        $http = new Client;
       // dd($request->all());
        try {
            $response = $http->post(config('services.passport.login_endpoint'), [
                'form_params' => [
                    'grant_type' => 'password',
                    'client_id' => config('services.passport.client_id'),
                    'client_secret' => config('services.passport.client_secret'),
                    'username' => $request->email,
                    'password' => $request->password,
                ]
            ]);
            return $response->getBody();
        } catch (Exception $e) {
            if ($e->getCode() === 400 || $e->getCode() === 401) {
                return response()->json([
                    'error' => 'Invalid credentials',
                    'message' => 'The user credentials were incorrect.'
                ], 401);
            }
            return response()->json([
                'message' => 'Something went wrong on the server.'
            ], $e->getCode());
        }
    }

    public function logout() {
        try {
            //\Auth::user()->tokens->each(func);
            \Auth::user()->token()->delete();
            return response()->json(['message' => 'User logged out'], 200);
        } catch (Exception $e) {
            if ($e->getCode() == 401) {
                return response()->json(['message' => 'User is not logged in'], $e->getCode());
            }
        }
    }
}
