<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Laravel\Scout\Searchable;

class Product extends Model
{
    use Searchable;

    protected $guarded = [];

    public function trader() {
        return $this->belongsTo('App\Trader');
    }

    public function orders() {
        return $this->belongsToMany('App\Order');
    }

    public function productImages() {
        return $this->hasMany('App\ProductImage');
    }

    public function states() {
        return $this->belongsToMany('App\State');
    }

    public function toSearchableArray()
    {
        $record = $this->load('states', 'productImages')->toArray();
        $record['stateIDs'] = [];
        foreach ($this->states as $state) {
            array_push( $record['stateIDs'], $state->id);
        }
        return $record;
    }
}
