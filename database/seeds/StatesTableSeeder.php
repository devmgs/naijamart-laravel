<?php

use Illuminate\Database\Seeder;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\State::class)->create(['name' => 'Abuja']);
        factory(App\State::class)->create(['name' => 'Kaduna']);
        factory(App\State::class)->create(['name' => 'Kano']);
        factory(App\State::class)->create(['name' => 'Lagos']);
    }
}
