<?php

use Illuminate\Database\Seeder;

class DialogTraderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(App\User::class)->create(['role' => 'trader']);
        $trader   = \App\Trader::create([
            'user_id' => $user->id,
            'full_name' => 'Dialog',
            'company_name' => 'Dialog',
            'profile_image' => 'dang_logo',
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986
        ]);
        for ($i = 0; $i < 15; $i++) {
            ProductHandler::handleAddition('Sample Product', 'Dialog', $trader->id, 6, 'sample-product.jpg');
        }
        ProductHandler::handleAddition('Panadol', 'Dialog', $trader->id, 6, 'dialog-panadol.png');
        ProductHandler::handleAddition('Strepsils', 'Dialog', $trader->id, 6, 'dialog-strepsils.jpg');
    }
}
