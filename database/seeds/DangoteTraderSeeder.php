<?php

use Illuminate\Database\Seeder;

class DangoteTraderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(App\User::class)->create(['role' => 'trader']);
        $trader   = \App\Trader::create([
            'user_id' => $user->id,
            'full_name' => 'Dangote',
            'company_name' => 'Dangote',
            'profile_image' => 'dang_logo',
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986
        ]);
        for ($i = 0; $i < 15; $i++) {
            ProductHandler::handleAddition('Sample Product', 'Dalema', $trader->id, 2, 'sample-product.jpg');
        }
        ProductHandler::handleAddition('Dangote Sugar', 'Dangote', $trader->id, 2, 'dangote-sugar.jpg');
        ProductHandler::handleAddition('Dangote Salt', 'Dangote', $trader->id, 2, 'dangote-salt.jpeg');
        ProductHandler::handleAddition('Dangote Flour', 'Dangote', $trader->id, 2, 'dangote-flour.jpeg');
        ProductHandler::handleAddition('Dangote Semolina', 'Dangote', $trader->id, 2, 'dangote-semolina.jpeg');

    }
}
