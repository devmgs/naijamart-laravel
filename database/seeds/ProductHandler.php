<?php


use Carbon\Carbon;

class ProductHandler
{
    public static function handleAddition($productName, $brand, $trader_id, $category_id, $imageLink) {
        $product = factory(App\Product::class)->create([
            'trader_id' => $trader_id,
            'category_id' => $category_id,
            'name' => $productName,
            'brand' => $brand,
            'description' => 'Lorem ipsum delorim',
            'price' => 2500,
        ]);
        App\ProductImage::create([
            'product_id' => $product->id,
            'link' => $imageLink
        ]);
        for ($i = 1; $i < 5; $i++ ) {
            $product->states()->attach($i, [
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
                'street_name_and_no' => 'No 1 Abuja Road',
                'lat' => 9.0765,
                'lng' => 7.3986,
                'amount_in_stock' => 100,
            ]);
        }
    }

}
