<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(OAuthClientsTableSeeder::class);
        $this->call(CategoriesTableSeeder::class);
        $this->call(StatesTableSeeder::class);
        $this->call(DangoteTraderSeeder::class);
        $this->call(SahadTraderSeeder::class);
        $this->call(DalemaTraderSeeder::class);
        $this->call(DialogTraderSeeder::class);
        //$this->call(TradersTableSeeder::class);

    }
}
