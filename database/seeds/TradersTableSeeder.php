<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class TradersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(App\User::class)->create(['role' => 'trader']);
        $trader   = \App\Trader::create([
            'user_id' => $user->id,
            'full_name' => 'Dangote',
            'company_name' => 'Dangote',
            'profile_image' => 'dang_logo',
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986
        ]);
        $product1 = factory(App\Product::class)->create([
            'trader_id' => $trader->id,
            'category_id' => 1,
            'name' => 'Dangote Sugar',
            'brand' => 'Dangote',
            'description' => 'Lorem ipsum delorim',
            'price' => 2500,
        ]);
        $product1->states()->attach(1, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product1->states()->attach(2, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product1->states()->attach(3, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product1->states()->attach(4, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);


        $product2 = factory(App\Product::class)->create([
            'trader_id' => $trader->id,
            'category_id' => 1,
            'name' => 'Dangote Sugar',
            'brand' => 'Dangote',
            'description' => 'Lorem ipsum delorim',
            'price' => 2500,
        ]);

        $product2 = factory(App\Product::class)->create([
            'trader_id' => $trader->id,
            'category_id' => 1,
            'name' => 'Dangote Tomato Paste',
            'brand' => 'Dangote',
            'description' => 'Lorem ipsum delorim',
            'price' => 2500,
        ]);

        $product2->states()->attach(1, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product2->states()->attach(2, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product2->states()->attach(3, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product2->states()->attach(4, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);


        $product3 = factory(App\Product::class)->create([
            'trader_id' => $trader->id,
            'category_id' => 1,
            'name' => 'Dangote Salt',
            'brand' => 'Dangote',
            'description' => 'Lorem ipsum delorim',
            'price' => 2500,
        ]);

        $product3->states()->attach(1, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product3->states()->attach(2, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product3->states()->attach(3, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product3->states()->attach(4, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);

        $product4 = factory(App\Product::class)->create([
            'trader_id' => $trader->id,
            'category_id' => 1,
            'name' => 'Dangote Tomato Paste',
            'brand' => 'Dangote',
            'description' => 'Lorem ipsum delorim',
            'price' => 2500,
        ]);

        $product4->states()->attach(1, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product4->states()->attach(2, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product4->states()->attach(3, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);
        $product4->states()->attach(4, [
            'created_at' => Carbon::now(),
            'updated_at' => Carbon::now(),
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986,
            'amount_in_stock' => 100,
        ]);

    }
}
