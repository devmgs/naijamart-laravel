<?php

use Illuminate\Database\Seeder;

class SahadTraderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $user = factory(App\User::class)->create(['role' => 'trader']);
        $trader   = \App\Trader::create([
            'user_id' => $user->id,
            'full_name' => 'Sahad',
            'company_name' => 'Sahad',
            'profile_image' => 'dang_logo',
            'street_name_and_no' => 'No 1 Abuja Road',
            'lat' => 9.0765,
            'lng' => 7.3986
        ]);
        for ($i = 0; $i < 15; $i++) {
            ProductHandler::handleAddition('Sample Product', 'Dalema', $trader->id, 2, 'sample-product.jpg');
        }
        ProductHandler::handleAddition('Indomie', 'Sahad', $trader->id, 2, 'sahad-indomie.jpeg');
        ProductHandler::handleAddition('Nido', 'Sahad', $trader->id, 2, 'sahad-nido.jpg');
        ProductHandler::handleAddition('Kellogs Cornflakes', 'Dalema', $trader->id, 2, 'sahad-cornflakes.jpg');
    }
}
