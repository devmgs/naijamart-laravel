<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Category::class)->create([
            'name' => 'perishables',
            'description' => 'Lorem Ipsum Delorim'
        ]);
        factory(App\Category::class)->create([
            'name' => 'provisions',
            'description' => 'Lorem Ipsum Delorim'
        ]);
        factory(App\Category::class)->create([
            'name' => 'toiletries',
            'description' => 'Lorem Ipsum Delorim'
        ]);
        factory(App\Category::class)->create([
            'name' => 'grains',
            'description' => 'Lorem Ipsum Delorim'
        ]);
        factory(App\Category::class)->create([
            'name' => 'clothing',
            'description' => 'Lorem Ipsum Delorim'
        ]);
        factory(App\Category::class)->create([
            'name' => 'medical',
            'description' => 'Lorem Ipsum Delorim'
        ]);
    }
}
