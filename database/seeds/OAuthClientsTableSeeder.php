<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class OAuthClientsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('oauth_clients')->insert([
            [
                'id' => 2,
                'user_id' => null,
                'name' => env('APP_NAME') . '1.0',
                'secret' => env('PASSPORT_CLIENT_SECRET'),
                'redirect' => env('APP_URL'),
                'personal_access_client' => 0,
                'password_client' => 1,
                'revoked' => 0,
                'created_at' => Carbon::now(),
                'updated_at' => Carbon::now(),
            ]
        ]);
    }
}
